# cloud-adventures-2
<figure align = "center">
<img src="images/banner2.png" alt="Trulli" width="800" height="425"">
</figure>

## Name
AWS Serverless app development with Fargate. 

## Description
Projects takes ideas from online courses, websites and articles to produce a working simple application.  It:
- Creates AWS Architectures in Terraform
- GitLab - CI/CD, Pipelines, Workspaces, AWS Integration and GitLab Flow.
- Serving a machine learning model with Fast API on AWS Fargate.

The project utilises:
- Application Load Balancer (ALB)
- Elastic Cloud Compute (EC2)
- Virtual Private Cloud (VPC)
- Subnets
- Gateways
- Simple Storage Service (S3)
- Route53
- Identity and Access Management (IAM)
- AWS Certificate Manager (ACM)
- Fargate
- Elastic Container Registry (ECR)
- Python machine learning libraries
- Fast API

The pipeline is defined in the `ci-cd.yml` file.  Work is done locally on branches and then committed to GitLab.  Tests are carried out and if successful merge and test are executed through the pipeline.       

The project is mostly based on a Terraform Module based course, given in the links below.  I stripped the modules down to resource based Terraform in order to learn the basics of Terraform.    

### Machine Learning model with FastAPI
As part of Udacity ML DevOps NanoDegree I developed a ML app. I used Git and DVC to track code, data and model while developing a simple classification model on the Census Income Data Set. After creating the model, it was finalized the model for production by checking its performance on slices and writing a model card encapsulating key knowledge about the model. Then I implemented a Continuous Integration and Continuous Deployment framework and ensured a pipeline passed a series of unit tests before deployment.

The full desciption and code is available here as [ML DevOps Model](https://github.com/edwards158/fastapi-heroku). I have used this model here and utilised FastAPI to serve the model on AWS Fargate.
The app files are located in the `my_api` folder:
<figure align = "left">
<img src="images/dir1.png" alt="app_folder" width="414" height="379"">
<figcaption align = "left"><b>App Folder Tree</b></figcaption>
</figure>


### Containers
I generated a Docker file to package the ML app.  This image was then built, tagged and pushed to AWS ECR by the pipeline.

### AWS Architecture
The AWS architecture was created and built in Terraform.  The code for this is given in the `deploy` folder.
The created architecture is shown in the Figure below.  The ML app is ran in Fargate and can be accessed through a custom DNS name.

### GitLab
GitLab Flow is used to produce CI/CD Pipeline based on main (staging) and production (release) branches.  Work is carried out locally on a feature branch pushed to GitLab on merge request which causes the pipeline to run.  Terraform workspaces are utilised to select staging and production builds.  A flow chart of the pipeline is shown below.  AWS information such as `AWS_ACCESS_KEY`, `AWS_SECRET_ACCESS_KEY` and `ECR_REPO` are stored in GitLab as hidden variables.

## GitFlow
<figure align = "center">
<img src="images/pipeline1.png" alt="Pipeline " width="800" height="865"">
<figcaption align = "center"><b>Pipleine flowchart</b></figcaption>
</figure>

## AWS Architecture

<figure align = "center">
<img src="images/CLOUD2.png" alt="AWS " width="800" height="636"">
<figcaption align = "center"><b>AWS Architecture</b></figcaption>
</figure>


## Usage
- Clone the project.  
- Obtain a DNS name or remove DNS based code to use Application balanacer arn to access the app.
- Use a policy to allow access to AWS resources.  A suitable file is given here.  Replace s3, ECR and other information with AWS relevant to you.  This [json file](https://gitlab.com/cloud-adventures/cloud-adventures-2/-/snippets/2307448) provides a policy to allow GitLab Pipelines access to AWS resources.
- When the architecture has been built navigate to DNS name or Load Balancer ARN to make predicitons with the app.   

## Results
<figure align = "center">
<img src="images/fastapi-images/fastapi-pic6.png" alt="AWS " width="800" height="438"">
<figcaption align = "center"><b>Healthy Target group of load bancer</b></figcaption>
</figure>

<br/><br/>

<figure align = "center">
<img src="images/fastapi-images/fastapi-pic5.png" alt="AWS " width="800" height="390"">
<figcaption align = "center"><b>Fargate Cluster info</b></figcaption>

<br/><br/>


</figure>
<figure align = "center">
<img src="images/fastapi-images/fastapi-pic1.png" alt="AWS " width="800" height="498"">
<figcaption align = "center"><b>POST response from FASTAPI at app.staging.cloudrichard.com</b></figcaption>
</figure>

<br/><br/>

</figure>
<figure align = "center">
<img src="images/fastapi-images/fastapi-pic7.png" alt="AWS " width="693" height="381"">
<figcaption align = "center"><b>Curl results from local machine to API endpoint on AWS</b></figcaption>
</figure>



## Contributing
This project is built upon cloud adventures 1 and its relevant contributing links.


## Project status
Project is ongoing:
~~- need to add unit tests for app in the pipeline.~~
