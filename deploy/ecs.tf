resource "aws_ecs_cluster" "main" {
  name = "${local.prefix}-cluster"

  tags = local.common_tags
}

data "template_file" "container_definitions" {
  template = file("../templates/ecs_container_definition.json")
  vars = {
    app_image        = var.ecr_image_api
    log_group_name   = aws_cloudwatch_log_group.ecs_task_log_group.name
    log_group_region = data.aws_region.current.name
    allowed_hosts    = aws_route53_record.app.fqdn

  }
}
resource "aws_ecs_task_definition" "app" {
  container_definitions    = data.template_file.container_definitions.rendered
  family                   = "${local.prefix}-app"
  cpu                      = 256
  memory                   = 512
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  execution_role_arn       = aws_iam_role.ecs_cluster_role.arn
  task_role_arn            = aws_iam_role.ecs_service_role.arn

  tags = local.common_tags
}
#service to run the application
resource "aws_ecs_service" "api" {
  name             = "${local.prefix}-app"
  cluster          = aws_ecs_cluster.main.name
  task_definition  = aws_ecs_task_definition.app.family
  desired_count    = 1
  launch_type      = "FARGATE"
  platform_version = "1.4.0"

  network_configuration {
    subnets = [aws_subnet.private_a.id,
      aws_subnet.private_b.id,
    ]
    security_groups = [aws_security_group.ecs_service.id]
  }
  load_balancer {
    target_group_arn = aws_alb_target_group.lb_target_group.arn
    container_name   = "cloud-2"
    container_port   = 8080
  }
  depends_on = [aws_lb_listener.api_https]
}

resource "aws_alb" "ecs_cluster_alb" {
  name               = "${local.prefix}-ecs-alb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.lb.id]
  subnets            = [aws_subnet.public_a.id, aws_subnet.public_b.id]

  tags = local.common_tags
}

resource "aws_alb_target_group" "lb_target_group" {
  name        = "${local.prefix}-lb-target-group"
  port        = 8080
  protocol    = "HTTP"
  vpc_id      = aws_vpc.main.id
  target_type = "ip"

  health_check {
    path                = "/"
    protocol            = "HTTP"
    matcher             = "200"
    interval            = "60"
    timeout             = "30"
    unhealthy_threshold = "3"
    healthy_threshold   = "3"

  }
  tags = local.common_tags

}
resource "aws_lb_listener" "alb_listener" {
  load_balancer_arn = aws_alb.ecs_cluster_alb.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}
resource "aws_lb_listener" "api_https" {
  load_balancer_arn = aws_alb.ecs_cluster_alb.arn
  port              = 443
  protocol          = "HTTPS"

  certificate_arn = aws_acm_certificate_validation.cert.certificate_arn

  default_action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.lb_target_group.arn
  }
}







