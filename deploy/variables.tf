variable "dns_name" {
  description = "my dns zone name"
  default     = "cloudrichard.com"
}
variable "subdomain" {
  description = "subdomain per environment"
  type        = map(string)
  default = {
    production = "app"
    staging    = "app.staging"
    dev        = "app.dev"
  }
}
variable "prefix" {
  default = "aws"
}
variable "project" {
  default = "cloud2"
}
variable "contact" {
  default = "email@maintainer.com"
}
variable "aws_region" {
  default = "us-east-1"
}
variable "vpc_cidr" {
  default = "10.0.0.0/16"
}
variable "pub_subnet_1_cidr" {
  default = "10.0.1.0/24"
}
variable "pub_subnet_2_cidr" {
  default = "10.0.2.0/24"
}
variable "priv_subnet_1_cidr" {
  default = "10.0.3.0/24"
}
variable "priv_subnet_2_cidr" {
  default = "10.0.4.0/24"
}
variable "azs" {
  type    = list(any)
  default = ["us-east-1a", "us-east-1b"]
}
variable "instance_type" {
  default = "t2.nano"
}
variable "all_routes" {
  description = "allow all ipv4 traffic"
  default     = "0.0.0.0/0"
}
variable "log_retention_in_days" {
  description = "ecs log retention duration"
  type        = number
  default     = 7
}
variable "ecr_image_api" {
  description = "ECR image for the app"
  default     = "962064455359.dkr.ecr.us-east-1.amazonaws.com/fast-api:latest"
}






