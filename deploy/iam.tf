
#role at start up for ecs
resource "aws_iam_role" "ecs_cluster_role" {
  name = "${local.prefix}-ecs-cluster-role"

  assume_role_policy = <<EOF
{
"Version": "2012-10-17",
"Statement": [
 {
   "Effect": "Allow",
   "Principal": {
     "Service": [   
                    "ecs-tasks.amazonaws.com",
                    "application-autoscaling.amazonaws.com"
                ]
   },
   "Action": "sts:AssumeRole"
  }
  ]
}
EOF
  tags               = local.common_tags
}
#policy attached to ecs on start up
resource "aws_iam_role_policy" "ecs_cluster_policy" {
  name = "${local.prefix}-ecs-cluster-policy"
  role = aws_iam_role.ecs_cluster_role.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
           "Effect": "Allow",
            "Action": [
                "ecr:GetAuthorizationToken",
                "ecr:BatchCheckLayerAvailability",
                "ecr:GetDownloadUrlForLayer",
                "ecr:BatchGetImage",
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ],
            "Resource": "*"
    }
  ]
}
EOF
}
#role for running containers
resource "aws_iam_role" "ecs_service_role" {
  name               = "${local.prefix}-ecs-service-role"
  assume_role_policy = <<EOF
{
  "Version": "2008-10-17",
  "Statement": [
    {
        "Action": "sts:AssumeRole",
        "Principal": {
        "Service": ["ecs.amazonaws.com", "ecs-tasks.amazonaws.com"]
      },
      "Effect": "Allow"
    }
  ]
}
EOF
  tags               = local.common_tags
}

resource "aws_cloudwatch_log_group" "ecs_task_log_group" {
  name              = "${local.prefix}-ecs-task-log-group"
  retention_in_days = var.log_retention_in_days
}
