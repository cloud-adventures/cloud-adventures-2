output "app_endpoint" {
  value = aws_route53_record.app.fqdn
}
output "aws_r53_name" {
  value = aws_route53_record.app.name
}

